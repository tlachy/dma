package hello.controller;

import hello.entity.Order;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by tlachy on 4.4.17.
 */


@RestController
@RequestMapping("/order")
public class OrderController {

    @GetMapping("/{id}")
    Order getOrderById(@PathVariable final String id) {
        //najdi order v databazi a vrat
        return new Order().setAmount(10d).setSymbol("eurusd").setType("buy");
    }

    //metoda co nastavi order

    //metodo ukonci order

    //atd...
}
