package hello.entity;

import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

/**
 * Created by tlachy on 4.4.17.
 */
@Getter
@Setter
@Accessors(chain = true)
public class Order {

    Long id = 2L;
    String symbol = "EUR/CZK";
    String type = "buy"; //or sell
    Double amount = 2D;
}
